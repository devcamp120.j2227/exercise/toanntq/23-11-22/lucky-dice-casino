// Khai báo thư viện mongo
const mongoose = require("mongoose")

//Khai báo class Schema
const Schema = mongoose.Schema

//Khởi tạo instance userSchema 
const userSchema = new Schema({
    username: { type: String, unique: true, required: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() },

    dicehistory: [{
        type: mongoose.Types.ObjectId,
        ref: "DiceHistory"
    }],
    prizehistory: [{
        type: mongoose.Types.ObjectId,
        ref: "PrizeHistory"
    }]


}, {
    //Lưu dấu bảng ghi được cập nhật vào thời gian nào
    timestamps: true
})

// Biên dịch một Book Model từ bookscheme
module.exports = mongoose.model("User", userSchema)